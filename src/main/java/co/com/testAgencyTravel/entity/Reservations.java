package co.com.testAgencyTravel.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RESERVATIONS")
public class Reservations implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_reservations")
	private Integer idReservations;

	@Column(name = "half_payment")
	private String halfPayment;

	@Column(name = "email")
	private String email;

	@Column(name = "document")
	private String document;

	@Column(name = "name_resident")
	private String nameResident;

	@Column(name = "birth_date")
	private String birthDate;

	@Column(name = "name_city")
	private String nameCity;
	
	@Column(name = "date_create")
	private Timestamp dateCreate;


	public Integer getIdReservations() {
		return idReservations;
	}

	public void setIdReservations(Integer idReservations) {
		this.idReservations = idReservations;
	}

	public String getHalfPayment() {
		return halfPayment;
	}

	public void setHalfPayment(String halfPayment) {
		this.halfPayment = halfPayment;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getNameResident() {
		return nameResident;
	}

	public void setNameResident(String nameResident) {
		this.nameResident = nameResident;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getNameCity() {
		return nameCity;
	}

	public void setNameCity(String nameCity) {
		this.nameCity = nameCity;
	}

	public Timestamp getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(Timestamp dateCreate) {
		this.dateCreate = dateCreate;
	}

		

}
