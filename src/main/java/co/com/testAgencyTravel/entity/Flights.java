package co.com.testAgencyTravel.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FLIGHTS")
public class Flights implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_flights")
	private Integer idFlights;

	@Column(name = "name_destination")
	private String nameDestination;

	@Column(name = "cost")
	private String cost;

	@Column(name = "ratings")
	private String ratings;

	@Column(name = "information")
	private String information;

	@Column(name = "urlImg")
	private String urlImg;

	@Column(name = "data_start")
	private Timestamp dataStart;

	@Column(name = "data_end")
	private Timestamp dataEnd;

	public Integer getIdFlights() {
		return idFlights;
	}

	public void setIdFlights(Integer idFlights) {
		this.idFlights = idFlights;
	}

	public String getNameDestination() {
		return nameDestination;
	}

	public void setNameDestination(String nameDestination) {
		this.nameDestination = nameDestination;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getRatings() {
		return ratings;
	}

	public void setRatings(String ratings) {
		this.ratings = ratings;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

	public Timestamp getDataStart() {
		return dataStart;
	}

	public void setDataStart(Timestamp dataStart) {
		this.dataStart = dataStart;
	}

	public Timestamp getDataEnd() {
		return dataEnd;
	}

	public void setDataEnd(Timestamp dataEnd) {
		this.dataEnd = dataEnd;
	}

}
