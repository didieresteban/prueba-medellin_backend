package co.com.testAgencyTravel.services;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import co.com.testAgencyTravel.Dao.CytiDAO;
import co.com.testAgencyTravel.Dao.FlightsDAO;
import co.com.testAgencyTravel.Dao.ReservationDAO;
import co.com.testAgencyTravel.entity.Cyti;
import co.com.testAgencyTravel.entity.Flights;
import co.com.testAgencyTravel.entity.Reservations;

@Controller
@RequestMapping("/travel")
public class TravelServices {

	@Autowired
	private FlightsDAO flightsDAO;

	@Autowired
	private CytiDAO cityDao;

	@Autowired
	private ReservationDAO reservationDAO;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	@CrossOrigin
	private Flights registerFlights(Flights flights) {
		return flightsDAO.save(flights);
	}

	@RequestMapping(value = "/reservation/save", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	@CrossOrigin
	private Reservations reservation(@RequestBody Reservations reservation) {
		reservation.setDateCreate(new Timestamp(new Date().getTime()));
		return reservationDAO.save(reservation);
	}

	@RequestMapping(value = "/find_flights", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@CrossOrigin
	private List<Flights> findAllFlights() {
		List<Flights> list = flightsDAO.findAll();
		for (Flights flights : list) {
			Date date = new Date();
			DateFormat hourFormat = new SimpleDateFormat("HH");
			int hour = Integer.parseInt(hourFormat.format(date));
			Calendar calendarioActual = Calendar.getInstance();
			calendarioActual.setTime(date);
			int diaSemana = calendarioActual.get(Calendar.DAY_OF_WEEK);
			if (diaSemana == 6 || diaSemana == 7) {
				Integer resultCalculo = (int) (Integer.parseInt(flights.getCost()) * 0.10);
				Integer sum = Integer.parseInt(flights.getCost()) + resultCalculo;
				flights.setCost(sum.toString());
				flightsDAO.save(flights);
			} else if (hour < 12) {
				Integer resultCalculo = (int) (Integer.parseInt(flights.getCost()) * 0.5);
				Integer sum = Integer.parseInt(flights.getCost()) + resultCalculo;
				flights.setCost(sum.toString());
				flightsDAO.save(flights);
			} else if (hour > 12) {
				Integer resultCalculo = (int) (Integer.parseInt(flights.getCost()) * 0.2);
				Integer rest = Integer.parseInt(flights.getCost()) - resultCalculo;
				flights.setCost(rest.toString());
				flightsDAO.save(flights);
			}

		}
		return flightsDAO.findAll();
	}

	@RequestMapping(value = "/reservation/{document}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@CrossOrigin
	private List<Reservations> findReservationID(@PathVariable("document") String document) {
		return reservationDAO.findByDocument(document);
	}

	@RequestMapping(value = "/city", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	@CrossOrigin
	private List<Cyti> findAllCyti() {
		return cityDao.findAll();
	}

}
