package co.com.testAgencyTravel.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.testAgencyTravel.entity.Cyti;

@Repository
public interface CytiDAO extends JpaRepository<Cyti, Integer> {

}
