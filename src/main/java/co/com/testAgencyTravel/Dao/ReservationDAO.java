package co.com.testAgencyTravel.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.com.testAgencyTravel.entity.Reservations;

@Repository
public interface ReservationDAO extends JpaRepository<Reservations, Integer>{

	@Query("select r from Reservations r where r.document = :document") 
	List<Reservations> findByDocument(@Param("document") String document);
}
