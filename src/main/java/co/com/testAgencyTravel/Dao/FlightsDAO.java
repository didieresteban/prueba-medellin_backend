package co.com.testAgencyTravel.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.testAgencyTravel.entity.Flights;

@Repository
public interface FlightsDAO extends JpaRepository<Flights, Integer> {

}
